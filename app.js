/*jshint esversion: 6 */
//--- import libraries
const express = require('express');//웹서버프레임워크
const bodyParser = require('body-parser');//http request paser
const path = require('path');//경로유틸리티
const axios = require('axios');//http request library(api 호출시 사용)
const jwt = require('jsonwebtoken');//인증처리 jwt줄임말--별도의 인증서버 필요없음
const cookieParser = require('cookie-parser');//쿠키처리 라이브러리
const util = require(__dirname + '/util');//custom library
//----------
//--- global constants & 환경변수
global.__BASEDIR = __dirname + '/';
global.__ACCESS_TOKEN_NAME = "x-access-token";//jwt http 헤더네임, 브라우저 쿠키 이름
global.__AUTH_API_URI = process.env.AUTH_API_URI || "http://login.169.56.170.186.nip.io";//인증 
global.__API_CONSULT_URI = process.env.API_CONSULT_URI || "http://rm-service.169.56.170.186.nip.io/consult";//개발서버api
global.__API_RM_SERVICE_URI = process.env.API_RM_SERVICE_URI || "http://rm-service.169.56.170.186.nip.io";//개발서버api
const JWT_SECRET = process.env.JWT_SECRET || "MySecretKey";//JWT 토큰을 해독하는 키
const port = (process.env.PORT || 8090);
//--------

//---- 기본 library 셋팅
const app = express();
app.use(express.static(path.join(__BASEDIR, '/public')));		//static resource 폴더 
app.use(bodyParser.urlencoded({ extended: false }));				//include request 객체 parser한글안깨지게
app.use(cookieParser());										//include cookie parser
//-----------

//--- ejs(Embed JS) 환경 셋팅
app.set('view engine', 'ejs');							//ui page rendering 시 ejs 사용
app.set('views', path.join(__BASEDIR, '/templates'));	//ui rendering시 사용할 ejs파일 위치 지정


var ejs = require('ejs');
/**
 * [EJS Filter : getContentCreateDateTime]
 * @param  {string} content []
 * @return {string}         []
 */
// ejs.filters.dateFormat = function(date) {

// 	var y = str.substr(0, 4);
// 	var m = str.substr(4, 2);
// 	var d = str.substr(6, 2);

// 	return y + "/" + m + "/" + d;
// }


//-------------
//----- middle ware: routing되는 서버모듈 시작 전에 항상 수행-인증토큰 검증
app.use(function (req, res, next) {
	let pathname = req.url;
	util.log("Request for [" + pathname + "] received.");

	//-- root path는 liveness, readiness probe임
	if (pathname === "/health") {
		res.writeHead(200, { 'Content-Type': 'text/html; charset=utf-8' });
		res.write('I am alive');
		res.end();
		next();
		return;
	}
	//--- Login page로 접근하는 경우는 처리 없이 진행
	if (pathname === "/login" || pathname === "/signup" || pathname === "/logout") {
		next();
		return;
	}
	next();
	//-- TODO - cookie가 아닌 Session에서 JWT KEY를 가져와, REDIS에서 해당 key의 JWT Token가져오도록 변경 필요
    /*
    let token = req.cookies[__ACCESS_TOKEN_NAME];
	if((typeof token == "undefined") || token == null) token = "";
	
    if(token === "") {
    	res.redirect("/login");
    	//next();
    	return;
    }
    
	util.log("## Verificate access Token=>"+token);
	
	jwt.verify(token, JWT_SECRET, function(err, decoded) {
		if (err) {
			//Token이 유효하지 않은 경우 Login페이지로 이동
			console.error(err);
			res.writeHead(200, { 'Content-Type':'text/html; charset=utf-8' });
			res.write('<h1>ID 또는 비밀번호가 틀립니다. 또는 인증서가 만료되었습니다.</h1><P><a href="/login">Try again</a>');
			res.end();
			next();
		} else {
			util.log("success to verify => " + JSON.stringify(decoded));
			util.userData.username = decoded.username;
			util.userData.name = decoded.name;
			next();
		}
	});
	*/

});//미들웨어, 사전처리기 

//-------------

//--- include 개발 모듈
app.use(require(path.join(__BASEDIR, "routes/auth.js")));		//include 인증처리 
app.use(require(path.join(__BASEDIR, "routes/consult.js")));		//rm 상담일지내역처리
//--------
//----- start web server 
app.listen(port, () => {
	console.log('Listen: ' + port);
});
//----------------
/**
 * apps.js 파일에 다음의 코드를 추가한다.
 */
app.use(function(req, res, next) {
	if (req.headers.hasOwnProperty('x-forwarded-for')) {
	   // proxy in effect
	   req.redirUrl = req.headers['x-forwarded-proto']
		  + "://"
		  + req.headers.host    // proxy
		  // plus any proxy subdirs if needed 
		  + "/"
		  + proxy_subdir
	   ;
  
	} else {
	   // direct requeset
	   req.redirUrl = req.protocol
		  + "://"
		  + req.headers.host
	   ;
	}
  
	next();
  });


  
  /**
 * redirect는 다음과 같이 사용한다.
 */
// res.redirect(req.redirUrl + "/redir_path");
