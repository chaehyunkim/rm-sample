/*jshint esversion: 6 */

//--- import libraries
const express = require('express');
const bodyParser = require('body-parser');
const axios = require('axios');
const cookieParser = require('cookie-parser');
const util = require(__BASEDIR + '/util');
//-----------

//---- 기본 library 셋팅
const router = express.Router();
router.use(bodyParser.urlencoded({ extended: false }));
router.use(cookieParser());








//--- 상담리스트화면 표시
router.get("/list", (req, res) => {
	util.log("list 화면");
	let body = {}
	showList(body, (data) => {
		// util.log(data);
		res.render("list", {
			consultList: data
		});
	});

});

//---------sample --------------------------
router.get("/getConsultNoteViewbyConsultId", (req, res) => {
	util.log("View 화면");
	let body = {}
	showList(body, (data) => {
		// util.log(data);
		res.render("list", {
			consultList: data
		});
	});

});

//--- 상담 화면 표시
router.get("/detail",  (req, res) => {
	let consultId = req.query.consultId;
	util.log("view 화면 : " + consultId);
	// let body = {};

	axios.get(__API_RM_SERVICE_URI + "/consult/getConsultNoteViewbyConsultId/" + consultId+"/")  // 서버에서 가져올 주소 매핑
		.then((ret) => {
			if (ret.status == 200) {
				util.log("consultId");
				// util.log(ret);
				res.render("detail", {
					data: ret.data // 화면에서 바인딩 되는 데이터 값
				});
			} else {
				// res.redirect("/detail");
			}
		})
		.catch((error) => {
			console.error(error);
			// res.redirect("/detail");
		});




});

router.get("/search", (req, res) => {
	let cond = req.query.cond;
	let keyword = req.query.keyword;

	let body = {}
	if (cond == "corp") {
		body.copNm = keyword;
	} else if (cond == "copCustNm") {
		body.copCustNm = keyword;
	} else if (cond == "branchNm") {
		body.branchNm = keyword;
	}
	showList(body, (data) => {
		util.log(data);
		res.render("list", {
			consultList: data,
			search_concept: cond,
			search_keyword: keyword
		});
	});
});


function showList(body, callback) {

	//--- 요청된 페이지 데이터 표시
	//let token = req.cookies[__ACCESS_TOKEN_NAME];
	let token = "hhhhh";
	let _headers = {};
	util.log("request token => " + token);
	_headers[__ACCESS_TOKEN_NAME] = token;


	axios.post(__API_CONSULT_URI + "/consultnotelistsearch",
		body,
		{
			headers: _headers
		})
		.then((ret) => {
			let data = ret.data;
			callback(data);
		})
		.catch((error) => {
			console.error("Fail to get entries", error);
		});
}

router.get("/toggle_star", (req, res) => {
	axios.put(__API_CONSULT_URI + "/consultnoteupdate/"+req.query.consultid+"/"+req.query.star, {
		//headers: _headers
	})
	.then((ret) => {
		console.log(ret.data);
		
	})
	.catch((error) => {
		console.error("Fail to put star", error);
	});

});    
module.exports = router;